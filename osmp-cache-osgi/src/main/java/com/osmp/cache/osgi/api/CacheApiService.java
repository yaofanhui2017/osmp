/*   
 * Project: OSMP
 * FileName: CacheApiService.java
 * version: V1.0
 */
package com.osmp.cache.osgi.api;

import com.osmp.intf.define.service.ConfigService;

/**
 * Description:
 * 
 * @author: wangkaiping
 * @date: 2014年12月2日 下午5:21:42
 */
public interface CacheApiService extends ConfigService {

}
