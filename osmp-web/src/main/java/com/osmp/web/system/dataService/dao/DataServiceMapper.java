package com.osmp.web.system.dataService.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.system.dataService.entity.DataService;

public interface DataServiceMapper extends BaseMapper<DataService> {

}
