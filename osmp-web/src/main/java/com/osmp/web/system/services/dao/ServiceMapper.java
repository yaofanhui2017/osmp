package com.osmp.web.system.services.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.system.services.entity.Service;

public interface ServiceMapper extends BaseMapper<Service> {

}
